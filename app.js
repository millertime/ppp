(function() {

$(document).ready(function() {

    var height = $(window).height();
    $(".content-box").css("min-height", height - 130);
    /* last page should be at least page height so menu works */
    $("#gallery").css("min-height", height - 40);

    var HOME_TOP = 0,
        ABOUT_TOP = height - 20,
        SERVICES_TOP = ABOUT_TOP + $("#about").innerHeight(),
        RATES_TOP = SERVICES_TOP + $("#services").innerHeight(),
        BLOG_TOP = RATES_TOP + $("#rates").innerHeight(),
        GALLERY_TOP = BLOG_TOP + $("#blog").innerHeight(),
        SOCIAL_TOP = height - 69;

    /* intialize social media links */
    $("#social-media").css("top", SOCIAL_TOP);

    function animateScrollToPos(pos) {
        $("html, body").stop(true,true).animate({scrollTop: pos}, 250);
    }

    var Router = Backbone.Router.extend({

        routes: {
            "home": "home",
            "about": "about",
            "services": "services",
            "rates": "rates",
            "blog": "blog",
            "gallery": "gallery"
        },
        home: function() {
            animateScrollToPos(HOME_TOP);
        },
        about: function() {
            animateScrollToPos(ABOUT_TOP);
        },
        services: function() {
            animateScrollToPos(SERVICES_TOP);
        },
        rates: function() {
            animateScrollToPos(RATES_TOP);
        },
        blog: function() {
            animateScrollToPos(BLOG_TOP);
        },
        gallery: function() {
            animateScrollToPos(GALLERY_TOP);
        }

    });

    $(window).scroll(function() {
        var windowPos = $(window).scrollTop();

        function selectMenuItem(id) {
            $(".menu-button.selected").removeClass("selected");
            $(".menu-button:eq(" + id + ")").addClass("selected");
        }

        /* Highlight menu items as you scroll */
        if (windowPos < ABOUT_TOP) {
            selectMenuItem(0);
        } else if (windowPos >= ABOUT_TOP && windowPos < SERVICES_TOP) {
            selectMenuItem(1);
        } else if (windowPos >= SERVICES_TOP && windowPos < RATES_TOP) {
            selectMenuItem(2);
        } else if (windowPos >= RATES_TOP && windowPos < BLOG_TOP) {
            selectMenuItem(3);
        } else if (windowPos >= BLOG_TOP && windowPos < GALLERY_TOP) {
            selectMenuItem(4);
        } else { /* windowPos >= GALLERY_TOP */
            selectMenuItem(5);
        }
    });

    // Start Backbone routing / history
    window.router = new Router();
    Backbone.history.start();
    // Initial redirect to 'home'
    if (!window.location.hash) {
        window.location = "/#/home";
    }

});

})();
